# Commands to manage eks cluster using eksctl utility
eksctl create cluster --name ${cluster_name} --version 1.17 --region ${AWS_REGION} --nodegroup-name ub-nodes --node-type t2.micro --nodes 2 --node-ami-family Ubuntu2004

eksctl delete cluster --name=${cluster_name}

eksctl scale nodegroup --cluster==${cluster_name} --region ${AWS_REGION} --name=ub-nodes --node-type t2.micro --nodes=1

# Versions used in the project
kubectl version = 1.23.6-00

Kubernetes version = 1.22

# Add EKS cluster detail in kube config file
aws eks --region ${AWS_REGION} update-kubeconfig --name ${cluster_name}

# Create secret for ECR registry
kubectl create secret docker-registry ecr-registry-secret --docker-server=343839505884.dkr.ecr.ap-south-1.amazonaws.com --docker-username=AWS --docker-password='aws ecr get-login-password --region ap-south-1 --no-include-email'

# Integrate Kubernetes dashboard with eks cluster
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml

kubectl create serviceaccount dashboard -n default

kubectl create clusterrolebinding dashboard-admin -n default --clusterrole=cluster-admin --serviceaccount=default:dashboard

kubectl get secret $(kubectl get serviceaccount dashboard -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode
