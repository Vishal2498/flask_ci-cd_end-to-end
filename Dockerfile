FROM python:3.10.6-alpine3.16

COPY . /app

WORKDIR /app

RUN pip3 install pipenv flask
RUN pipenv install --dev

EXPOSE 5000

WORKDIR /app/src

ENTRYPOINT [ "python3" ]

CMD [ "sample_flask_app.py" ]
