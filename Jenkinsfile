pipeline {
    environment {
        PATH="/home/ubuntu/.local/bin:${env.PATH}"
        scannerHome = tool 'dexter-sonarscanner'
        GIT_URL = 'http://ec2-3-7-66-26.ap-south-1.compute.amazonaws.com/dexter/python-hello-world'
        AWS_REGION = 'ap-south-1'
        ACCESS_KEY = credentials('aws_access_key')
        SECRET_KEY = credentials('aws_secret_key')
        AWS_ACCOUNT_ID = credentials('aws_account_id')
        AWS_CLUSTER_NAME = 'dexter-application-cluster'
        ECR_repo_name = 'dexter-flask-project'
        GITHUB_CREDENTIALS=credentials('dexter-gitlab-cred')
        BLUE_DEPLOYMENT='flask-app-deployment-blue'
        GREEN_DEPLOYMENT='flask-app-deployment-green'
        SERVICE='flask-app'
        HEALTH_CHECK_URL='http://ae8a522936c654a5f9d9b29d2c1891ae-1225690160.ap-south-1.elb.amazonaws.com:5000/health'
        EXPECTED_RESPONSE='OK'
    }
    agent {
        label 'demo'
    }
    stages {
        stage('Checkout') {
            steps {
                script {
                    if (env.CHANGE_ID) {
                        def branch = env.gitlabSourceBranch
                        echo "Webhook triggered from branch: ${branch}"
                        git branch: "${branch}", url: "${env.GIT_URL}"
                    }
                    else {
                        env.branch = 'development'
                        println(env.GIT_URL)
                        git branch: "${branch}", url: "${env.GIT_URL}"
                    }
                    env.GIT_BRANCH = branch
                    
                    echo '===Configuring Repository on the system==='
                    sh 'git remote set-url origin http://${GITHUB_CREDENTIALS_USR}:${GITHUB_CREDENTIALS_PSW}@ec2-3-7-66-26.${AWS_REGION}.compute.amazonaws.com/dexter/python-hello-world.git'
                    sh 'git push --set-upstream origin ${GIT_BRANCH}'
                }
            }
        }
        
        stage('Prerequisits') {
            steps {
                sh 'pipenv install --dev'
            }
        }
        
        stage('Unit test') {
            steps {
                sh 'pipenv run coverage run -m unittest discover -b'
            }
        }
        
        stage('Code Coverage') {
            steps {
                sh '''
                pipenv run coverage report -m  --include="src/*" --omit="*/test/*,src/__init__*"
                pipenv run coverage xml
                '''
            }
        }
        
        stage('Sonar Analysis') {
            steps {
                script {
                    withSonarQubeEnv('dexter-sonarqube') {
                        sh '${scannerHome}/bin/sonar-scanner -X -Dproject.settings=./sonar-scanner.properties'
                    }
                    
                    timeout(time: 10, unit: 'MINUTES') {
                        waitForQualityGate abortPipeline: true, credentialsId: 'dexter-sonarqube'
                    }
                }
            }
        }
        
        stage('Incrementing Tag') {
            steps {
                script {
                    def currentVersion = sh(returnStdout: true, script: 'git describe --abbrev=0 --tags').trim()
                    def(v,Major,Minor,Patch) = currentVersion.split('\\.')
                    if (env.GIT_BRANCH == 'development') {
                        newPatch = Patch.toInteger() + 1
                        env.newVersion = "${v}.${Major}.${Minor}.${newPatch}"
                    }
                    else if (env.GIT_BRANCH == 'staging') {
                        newMinor = Minor.toInteger() + 1
                        env.newVersion = "${v}.${Major}.${newMinor}.0"
                    }
                    else {
                        newMajor = Major.toInteger() + 1
                        env.newVersion = "${v}.${newMajor}.0.0"
                    }
                    echo "===Creating new git tag==="
                    prevCommitMessage = sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
                    sh "git tag ${newVersion} -m '${prevCommitMessage}'"
                }
            }
        }
        
        stage ('Build and Tag Image') {
            steps {
                echo "===Building Docker Image==="
                sh 'docker build -t my-flask-app .'
                
                echo "===Tagging Docker Image==="
                sh 'docker tag my-flask-app:latest ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${ECR_repo_name}:${newVersion}'
            }
        }
        
        stage('Push Image to ECR') {
            steps {
                echo "===Configuring aws cli==="
                sh "aws configure set aws_access_key_id ${ACCESS_KEY}"
                sh "aws configure set aws_secret_access_key ${SECRET_KEY}"
                sh 'aws configure set default.region ${AWS_REGION}'
                sh 'aws configure set default.output json'
                
                echo "===Logging to ECR==="
                sh 'aws ecr get-login-password --region ${AWS_REGION} | docker login --username AWS --password-stdin ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com'
                
                echo "===Pushing Image to ECR==="
                sh 'docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${ECR_repo_name}:${newVersion}'

                echo "===Removing Image from local machine==="
                sh 'docker rmi -f my-flask-app:latest'
                sh 'docker rmi -f ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${ECR_repo_name}:${newVersion}'
            }
        }
        
        stage('Deploy to EKS Cluster') {
            steps {
                script {
                    echo "===Configuring kube config file==="
                    sh "aws eks --region ${AWS_REGION} update-kubeconfig --name ${AWS_CLUSTER_NAME}"
                    
                    echo "===Starting Blue-Green Deployment==="
                    env.CURRENT_ENV = sh(returnStdout: true, script: "kubectl get service ${SERVICE} -o jsonpath='{.spec.selector.environment}'").trim()
                    
                    if (env.CURRENT_ENV == 'blue') {
                        env.DEPLOYMENT_NAME=env.GREEN_DEPLOYMENT
                        env.NEW_ENV='green'
                        sh 'sed -i "s|:latest|:${newVersion}|" ./K8s/green-deployment.yaml'
                    }
                    else {
                        env.DEPLOYMENT_NAME=env.BLUE_DEPLOYMENT
                        env.NEW_ENV='blue'
                        sh 'sed -i "s|:latest|:${newVersion}|" ./K8s/blue-deployment.yaml'
                    }
                        
                    if(CURRENT_ENV == 'blue') {
                        sh 'kubectl apply -f ./K8s/green-deployment.yaml'
                    }
                    else {
                        sh 'kubectl apply -f ./K8s/blue-deployment.yaml'
                    }
                    
                    def startTime = new Date().time
                    def endTime = startTime + (5 * 60000)
                    def desired_pods = sh(returnStdout: true, script: "kubectl get deployment ${DEPLOYMENT_NAME} -o jsonpath='{.spec.replicas}'").trim()
                    
                    while (new Date().time < endTime) {
                        def running_pods = sh(returnStdout: true, script: '''kubectl get pods --selector=environment=${NEW_ENV} --no-headers | awk '$3 == "Running"' | wc -l''').trim()
                        if (![running_pods == desired_pods]) {
                            error('Pipeline aborted: Unable to deploy new object')
                        }
                        echo "Waiting for pods to be ready..."
                        if (running_pods == desired_pods) {
                            echo "All required pods are up and running"
                            break
                        }
                        sh 'sleep 10'
                    }
                    echo 'Switching to new service'
                    sh '''
                    kubectl patch service ${SERVICE} -p '{"spec": {"selector": {"environment": "'${NEW_ENV}'"}}}'
                    '''
                    echo "Traffic switched to $DEPLOYMENT_NAME"
                
                }
            }
        }
        
        stage('Health Check/Roll back') {
            steps {
                script {
                    echo '===Performing Health check==='
                    
                    def response = sh 'curl -s ${HEALTH_CHECK_URL}'
                    println(response)
                    
                    if (![response == EXPECTED_RESPONSE]) {
                        env.HEALTH_CHECK="FAIL"
                        echo "Health check failed: ${response}"
                    }
                    env.HEALTH_CHECK="PASS"
                    
                    if (HEALTH_CHECK == 'FAIL') {
                        echo "New deployment is not healthy, rolling back to previous deployment"
                        echo 'Switching to new service'
                        sh '''
                        kubectl patch service ${SERVICE} -p '{"spec": {"selector": {"environment": "'${CURRENT_ENV}'"}}}'
                        '''
                        echo "Traffic switched to ${DEPLOYMENT_NAME}"
                        
                    }
                }
            }
        }
        
        stage('Cleanup') {
            steps {
                script {
                    echo '===Cleaning up deployment as per status==='
                    if (HEALTH_CHECK == "True") {
                        if (DEPLOYMENT_NAME == BLUE_DEPLOYMENT) {
                            sh 'kubectl delete deployment ${GREEN_DEPLOYMENT}'
                        }
                        else {
                            sh 'kubectl delete deployment ${BLUE_DEPLOYMENT}'
                        }
                    }
                    else {
                        if (DEPLOYMENT_NAME == BLUE_DEPLOYMENT) {
                            sh 'kubectl delete deployment ${BLUE_DEPLOYMENT}'
                        }
                        else {
                            sh 'kubectl delete deployment ${GREEN_DEPLOYMENT}'
                        }
                    }
                    
                    if (env.HEALTH_CHECK == 'FAIL') {
                        error("Pipeline aborted: Health check failed for new deployment")
                    }
                }
            }
        }
        
        stage('Pushing Tag') {
            steps {
                echo "===Pushing latest lag to Repoistory==="
                sh 'git push --follow-tags'
            }
        }
    }
    post {
        always {
            script {
                echo "===Removing git config file==="
                sh 'rm -rf .git/config'
                
                echo "===Removing aws credentials==="
                sh 'rm -rf ~/.aws/credentials'
                
                echo "===Removing kube config file==="
                sh 'rm -rf ~/.kube/config'
                
                echo "===Logout from ECR==="
                sh 'docker logout'
                
                echo "Clearing all tags locally"
                sh 'git tag -d $(git tag -l)'
            }
        }
        success {
            script {
                echo "PIPELINE SUCCESSFULL"
            }
        }
    }
}
